

enable_testing()


set(TESTLIST
   AABB
   Brush
)



add_custom_target(test 
   ALL 
   COMMAND ${CMAKE_CTEST_COMMAND} --output-on-failure)


foreach(TEST ${TESTLIST})
   set(TEST_EXE test_${TEST})
   add_executable(${TEST_EXE} ${TEST}.c)
   target_link_libraries(${TEST_EXE} PUBLIC 
      ryanmock 
      ryancsglib)
   add_test(
      NAME ${TEST}
      COMMAND ${TEST_EXE} --random)
      
   add_dependencies(test ${TEST_EXE})
   

   #add_custom_target(${TEST_EXE}_RUN ALL
   #                  ${TEST_EXE}
   #                  WORKING_DIRECTORY
   #                  DEPENDS ${TEST_EXE})
endforeach()


