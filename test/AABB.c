#include <ryanmock.h>
#include <AABB.h>

void test_RayCast_Miss()
{
   struct aabb aabb;
   struct ray3f ray;
   AABB_Set(&aabb, 0, 0, 0, 1, 1, 1);

   Ray3f_Init2(&ray, 2, 2, 2, 1, 1, 1);
   rmmAssertFalse(AABB_RayCast(&aabb, &ray));
   
   Ray3f_Init2(&ray, -1, -1, -1, -1, -1, -1);
   rmmAssertFalse(AABB_RayCast(&aabb, &ray));
   
}

void test_RayCast_Hit()
{
   struct aabb aabb;
   struct ray3f ray;
   AABB_Set(&aabb, 0, 0, 0, 1, 1, 1);

   Ray3f_Init2(&ray, -3, -3, -3, 1, 1, 1);
   rmmAssertTrue(AABB_RayCast(&aabb, &ray));
   
   Ray3f_Init2(&ray, 3, 3, 3, -1, -1, -1);
   rmmAssertTrue(AABB_RayCast(&aabb, &ray));
   
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_RayCast_Miss),
      rmmMakeTest(test_RayCast_Hit),
   };
   return rmmRunTestsCmdLine(tests, "AABB", argc, args);
}


