#include <ryanmock.h>
#include <Brush.h>
#include <Ray3f.h>

static 
bool (*RayCastToFace)(const struct ray3f * ray,
                      const struct point * points,
                      const int * faceIndicies,
                      size_t faceIndiciesCount,
                      struct vec3f * outPoint);

RMM_SYMBOLTABLE_LOAD(Brush);

static
void rmstate(enum ryanmock_state state,
             const struct ryanmock_test * test)
{
   if(state == eRMS_SuiteSetup)
   {
      rmmSymbolTableLink(Brush, RayCastToFace);
   }
}
#define MAX_FACE_POINTS 32

struct faceData
{
   size_t count;
   struct point points[MAX_FACE_POINTS];
   int indices[MAX_FACE_POINTS];
};

static inline
void LoadFaceData(struct faceData * data, const struct vec3f * vecs, size_t count)
{
   size_t i;
   data->count = count;
   for(i = 0; i < count; i++)
   {
      Vec3f_Copy(&data->points[i].p, &vecs[i]);
      data->indices[i] = i;
   }
}
#define LoadLocalFaceData(data, vecs) \
LoadFaceData(data, vecs, sizeof(vecs) / sizeof(struct vec3f))

#define assertVec3f(v, x, y, z) \
rmmEnterFunction(assertVec3f);  \
_assertVec3f(v, x, y, z);       \
rmmExitFunction(assertVec3f)


static inline
void _assertVec3f(const struct vec3f * v, float x, float y, float z)
{
   rmmAssertFloatWithin(v->x, x, 0.0001);
   rmmAssertFloatWithin(v->y, y, 0.0001);
   rmmAssertFloatWithin(v->z, z, 0.0001);
}

static
void test_RayCastToFace_PerpendicularMiss()
{
   const struct vec3f faceVects[] = {
      {0,0,0}, {1, 0, 0}, {0, 1, 0},
   };
   struct ray3f ray;
   struct faceData faceData;
   struct vec3f outPoint;
   LoadLocalFaceData(&faceData, faceVects);
   Ray3f_Init2(&ray, 3, 3, 3, 1, 1, 1);

   rmmAssertFalse(RayCastToFace(&ray, faceData.points, faceData.indices, faceData.count, &outPoint));

   Ray3f_Init2(&ray, 0.6, 0.6, -3, 0, 0, 1);

   rmmAssertFalse(RayCastToFace(&ray, faceData.points, faceData.indices, faceData.count, &outPoint));
   
   
}

static
void test_RayCastToFace_PerpendicularHit()
{
   const struct vec3f faceVects[] = {
      {0,0,0}, {1, 0, 0}, {0, 1, 0},
   };
   struct ray3f ray;
   struct faceData faceData;
   struct vec3f outPoint;
   LoadLocalFaceData(&faceData, faceVects);
   
   Ray3f_Init2(&ray, 0.4, 0.4, -3, 0, 0, 1);
   rmmAssertTrue(RayCastToFace(&ray, faceData.points, faceData.indices, faceData.count, &outPoint));
   assertVec3f(&outPoint, 0.4, 0.4, 0);
   
   Ray3f_Init2(&ray, 0.4, 0.4, 3, 0, 0, -1);
   rmmAssertTrue(RayCastToFace(&ray, faceData.points, faceData.indices, faceData.count, &outPoint));
   assertVec3f(&outPoint, 0.4, 0.4, 0);
   
}

static
void test_RayCastToFace_ParallelMiss()
{
   const struct vec3f faceVects[] = {
      {0,0,0}, {1, 0, 0}, {0, 1, 0},
   };
   struct ray3f ray;
   struct faceData faceData;
   struct vec3f outPoint;
   LoadLocalFaceData(&faceData, faceVects);
   Ray3f_Init2(&ray, 3, 3, 3, 1, 0, 0);

   rmmAssertFalse(RayCastToFace(&ray, faceData.points, faceData.indices, faceData.count, &outPoint));

   
   
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_RayCastToFace_PerpendicularMiss),
      rmmMakeTest(test_RayCastToFace_PerpendicularHit),
      rmmMakeTest(test_RayCastToFace_ParallelMiss),
   };
   return rmmRunTestsCmdLine2(tests, "Brush", rmstate, argc, args);
}


