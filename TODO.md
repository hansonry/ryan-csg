# TODO

## Near Term

[ ] Finish Brush Ray Intersection
    [ ] Find Closest Point between Line and Ray
    [ ] Figure out if that point is withing the points of the edge.
    [ ] Find closest valid point to the ray origin (This is the point we want)
    [ ] Some testing of this

## Far Term

* CSG Gemoetry
* Rendering Textures
* Raytracing Texture Generator

