#ifndef __RAY3F_H__
#define __RAY3F_H__

#include "Vec3f.h"

struct ray3f
{
   struct vec3f origin;
   struct vec3f dir;
};

static inline
struct ray3f * Ray3f_Init(struct ray3f * ray, const struct vec3f * origin, 
                                              const struct vec3f * dir)
{
   Vec3f_Copy(&ray->origin, origin);
   Vec3f_Copy(&ray->dir,    dir);
   return ray;
}

static inline
struct ray3f * Ray3f_Init2(struct ray3f * ray, float originX, float originY, float originZ, 
                                               float dirX, float dirY, float dirZ)
{
   Vec3f_Set(&ray->origin, originX, originY, originZ);
   Vec3f_Set(&ray->dir,    dirX,    dirY,    dirZ);
   return ray;
}

static inline
struct ray3f * Ray3f_Copy(struct ray3f * dest, const struct ray3f * src)
{
   Vec3f_Copy(&dest->origin, &src->origin);
   Vec3f_Copy(&dest->dir,    &src->dir);
   return dest;
}


#endif // __RAY3F_H__

