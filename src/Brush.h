#ifndef __BRUSH_H__
#define __BRUSH_H__

#include "Point.h"

struct brush;

struct brush * Brush_Create(void);
void Brush_Destroy(struct brush * brush);


void Brush_AddFace(struct brush * brush, const struct point * points, 
                   size_t pointsCount);

void Brush_Translate(struct brush * brush, const struct vec3f * by);


void Brush_WriteObj(const struct brush * brush, const char * filename);


struct brush * Brush_Create_Cube(const struct vec3f * min, 
                                 const struct vec3f * max);


void Brush_BuildBVH(struct brush * brush);

#endif //__BRUSH_H__

