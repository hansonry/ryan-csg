#ifndef __VEC3F_H__
#define __VEC3F_H__

struct vec3f
{
   float x;
   float y;
   float z;
};

static inline
struct vec3f * Vec3f_Copy(struct vec3f * dest, const struct vec3f * src)
{
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
   return dest;
}

static inline
struct vec3f * Vec3f_Set(struct vec3f * v, float x, float y, float z)
{
   v->x = x;
   v->y = y;
   v->z = z;
   return v;
}

static inline
struct vec3f * Vec3f_Zero(struct vec3f * v)
{
   v->x = 0;
   v->y = 0;
   v->z = 0;
   return v;
}




struct vec3f * Vec3f_Add(struct vec3f * out, 
                         const struct vec3f * a, 
                         const struct vec3f * b);

struct vec3f * Vec3f_Subtract(struct vec3f * out, 
                              const struct vec3f * a, 
                              const struct vec3f * b);

float Vec3f_Length2(const struct vec3f * v);
float Vec3f_Length(const struct vec3f * v);

struct vec3f * Vec3f_Normalize(struct vec3f * out, 
                               const struct vec3f * v);



static inline
float Vec3f_Dot(const struct vec3f * a, 
                const struct vec3f * b)
{
   return a->x * b->x +
          a->y * b->y +
          a->z * b->z;
}

static inline
struct vec3f * Vec3f_Cross(struct vec3f * out,
                           const struct vec3f * a,
                           const struct vec3f * b)
{
   out->x = a->y * b->z - a->z * b->y;
   out->y = a->z * b->x - a->x * b->z;
   out->z = a->x * b->y - a->y * b->x;
   return out;
}

static inline
struct vec3f * Vec3f_SafeCross(struct vec3f * out,
                               const struct vec3f * a,
                               const struct vec3f * b)
{
   struct vec3f temp;
   (void)Vec3f_Cross(&temp, a, b);
   return Vec3f_Copy(out, &temp);
}

static inline
struct vec3f * Vec3f_ScaleComps(struct vec3f * out, 
                                const struct vec3f * a,
                                const struct vec3f * b)
{
   out->x = a->x * b->x;
   out->y = a->y * b->y;
   out->z = a->z * b->z;
   return out;
}

static inline
struct vec3f * Vec3f_Scale(struct vec3f * out,
                           const struct vec3f * a, 
                           float b)
{
   out->x = a->x * b;
   out->y = a->y * b;
   out->z = a->z * b;
   return out;
}


#endif // __VEC3F_H__

