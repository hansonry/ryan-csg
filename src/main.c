#include <stdio.h>
#include "Brush.h"


int main(int argc, char * args[])
{
   (void)argc;
   (void)args;
   struct vec3f min = { -1, -1, -1 };
   struct vec3f max = {  1,  1,  1 };
   struct brush * brush = Brush_Create_Cube(&min, &max);
   Brush_BuildBVH(brush);
   Brush_WriteObj(brush, "test.obj");
   Brush_Destroy(brush);
   printf("End of Program.\n");
   return 0;
}
