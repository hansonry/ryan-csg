#include "Vec3f.h"
#include <math.h>

struct vec3f * Vec3f_Add(struct vec3f * out, 
                         const struct vec3f * a, 
                         const struct vec3f * b)
{
   out->x = a->x + b->x;
   out->y = a->y + b->y;
   out->z = a->z + b->z;
   return out;
}

struct vec3f * Vec3f_Subtract(struct vec3f * out, 
                              const struct vec3f * a, 
                              const struct vec3f * b)
{
   out->x = a->x - b->x;
   out->y = a->y - b->y;
   out->z = a->z - b->z;
   return out;
}

static inline
float length2(const struct vec3f * v)
{
   return v->x * v->x + v->y * v->y + v->z * v->z;
}

float Vec3f_Length2(const struct vec3f * v)
{
   return length2(v);
}

float Vec3f_Length(const struct vec3f * v)
{
   return sqrt(length2(v));
}

struct vec3f * Vec3f_Normalize(struct vec3f * out, 
                               const struct vec3f * v)
{
   float length = Vec3f_Length(v);
   if(length < 0.0001)
   {
      out->x = 0;
      out->y = 0;
      out->z = 0;
   }
   else
   {
      out->x = v->x / length;
      out->y = v->y / length;
      out->z = v->z / length;
   }
   return out;
}

