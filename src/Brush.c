#include <stdio.h>
#include <stdlib.h>

#include "Brush.h"
#include "AABB.h"
#include "List.h"

LIST_MAKE(point, struct point)
LIST_MAKE(index, int)


struct bvhNode
{
   struct bvhNode * left;
   struct bvhNode * right;
   struct aabb aabb;
   int faceStartIndex;
};


LIST_MAKE(node, struct bvhNode * )

struct brush
{
   struct list_point points;
   struct list_index indices;
   struct bvhNode * root;
};

static inline
void BVHNode_Print(const struct bvhNode * node, const char * type, int indent)
{
   if(node != NULL)
   {
      int indentSpace = indent * 2;
      printf("%*.s%p %s\n", indentSpace, "", (void*)node, type);
      BVHNode_Print(node->left, "left", indent + 1);
      BVHNode_Print(node->right, "right", indent + 1);
   }
}

struct brush * Brush_Create(void)
{
   struct brush * brush = malloc(sizeof(struct brush));
   List_Init_point(&brush->points, 0, 0);
   List_Init_index(&brush->indices, 0, 0);
   brush->root = NULL;
   return brush;
}

static inline
void BVHTree_Destory(struct bvhNode * node)
{
   if(node->left != NULL)
   {
      BVHTree_Destory(node->left);
   }
   if(node->right != NULL)
   {
      BVHTree_Destory(node->right);
   }
   free(node);
}

void Brush_Destroy(struct brush * brush)
{
   List_Free_point(&brush->points);
   List_Free_index(&brush->indices);
   if(brush->root != NULL)
   {
      BVHTree_Destory(brush->root);
   }
}

static inline
bool BVHTree_IsLeaf(const struct bvhNode * node)
{
   return node->left == NULL && node->right == NULL;
}

static inline
struct bvhNode * BVHNode_Create(void)
{
   struct bvhNode * node = malloc(sizeof(struct bvhNode));
   node->left = NULL;
   node->right = NULL;
   AABB_Empty(&node->aabb);
   node->faceStartIndex = -1;
   return node;
}

static inline
int Brush_BuildBVH_SortList(struct list_node * nodeList,
                            int startIndex, int endIndex,
                            const struct aabb * left,
                            const struct aabb * right)
{
   enum aabbIntersection leftI, rightI;
   const struct bvhNode ** leafs = List_GetC_node(nodeList, NULL);
   bool keep = true;

   
   while(startIndex < endIndex)
   {
      leftI  = AABB_Intersection(&leafs[startIndex]->aabb, left);
      rightI = AABB_Intersection(&leafs[startIndex]->aabb, right);
      bool onLeft = (leftI == eAABBI_Inside || 
                     (leftI == eAABBI_Intersection && 
                      rightI == eAABBI_NoIntersection));
      // If intersecting both. Alternate who gets to own it
      if(!onLeft && leftI == eAABBI_Intersection && rightI == eAABBI_Intersection)
      {
         onLeft = keep;
         keep = !keep;
      }
      // Swap and ajust pointers and indices as nessary
      if(onLeft)
      {
         startIndex ++;
      }
      else
      {
         endIndex --;
         List_Swap_node(nodeList, startIndex, endIndex);
      }
   }
   return startIndex;

}

static inline
struct bvhNode * Brush_BuildBVH_CreateNode(struct list_node * nodeList,
                                           int startIndex, int endIndex)
{
   struct bvhNode * node = BVHNode_Create();
   const struct bvhNode ** leafs = List_GetC_node(nodeList, NULL);
   int i;
   for(i = startIndex; i < endIndex; i++)
   {
      AABB_Merge(&node->aabb, &node->aabb, &leafs[i]->aabb); 
   } 
   return node;
}

static inline
void Brush_BuildBVH_Step(struct bvhNode * node,
                         const struct aabb * bounds, 
                         struct list_node * nodeList, 
                         int startIndex, int endIndex)
{
   struct aabb a, b;
   struct bvhNode ** leafs = List_Get_node(nodeList, NULL);
   AABB_SplitOnLongestAxis(bounds, &a, &b);
   int firstRightIndex = Brush_BuildBVH_SortList(nodeList, startIndex, endIndex,
                                                 &a, &b);
   if(firstRightIndex > startIndex)
   {
      if(firstRightIndex - startIndex == 1)
      {
         node->left = leafs[startIndex];
      } 
      else
      {
         node->left = Brush_BuildBVH_CreateNode(nodeList, startIndex, firstRightIndex);
         Brush_BuildBVH_Step(node->left, &a, nodeList, startIndex, firstRightIndex);
      }
   }   
   if(firstRightIndex < endIndex)
   {
      if(endIndex - firstRightIndex == 1)
      {
         node->left = leafs[firstRightIndex];
      } 
      else
      {
         node->right = Brush_BuildBVH_CreateNode(nodeList, firstRightIndex, endIndex);
         Brush_BuildBVH_Step(node->right, &b, nodeList, firstRightIndex, endIndex);
      }
   }
}


void Brush_BuildBVH(struct brush * brush)
{
   size_t i, count;
   const struct point * points = List_GetC_point(&brush->points, NULL);
   const int * indices = List_GetC_index(&brush->indices, &count);
   struct list_node nodeList;
   struct bvhNode * node = NULL;
   brush->root = BVHNode_Create();
   List_Init_node(&nodeList, 0, 0);
   
   // For each face, create a leaf node and add it to the root'
   // brush aabb

   for(i = 0; i < count; i++)
   {
      int index = indices[i];
      if(node == NULL)
      {
         node = BVHNode_Create();
         List_Add_node(&nodeList, node, NULL);
         node->faceStartIndex = i;
      }
      if(index == -1)
      {
         AABB_Merge(&brush->root->aabb, &brush->root->aabb, &node->aabb); 
         node = NULL;
      }
      else
      {
         AABB_AddPoint(&node->aabb, &points[index].p);
      }
   }

   // Start subdividing the root aabb into parts
   Brush_BuildBVH_Step(brush->root, &brush->root->aabb, 
                       &nodeList, 0, List_GetCount_node(&nodeList));

   List_Free_node(&nodeList);

}

struct brushRayCastResults
{
   const struct brush * brush;
   const struct ray3f * ray;
   unsigned int hitCount;
   struct vec3f point;
};

static inline
int GetSign(float f)
{
   if(fabs(f) < 0.00001)
   {
      return 0;
   }
   if(f < 0)
   {
      return -1;
   }
   return 1;
}

static inline
bool RayCastToFace_IsPointInsideFace(const struct point * points,
                                     const int * faceIndicies,
                                     size_t faceIndiciesCount,
                                     const struct vec3f * faceNormal,
                                     const struct vec3f * pointToCheck)
{
   size_t prevIndex = faceIndiciesCount - 1;
   size_t index;
   int expectedSign = 0;
   
   for(index = 0; index < faceIndiciesCount; index++)
   {
      struct vec3f edge;
      struct vec3f toPoint;
      struct vec3f cross;
      float dot;
      int sign;
      Vec3f_Subtract(&edge, &points[faceIndicies[index]].p,
                            &points[faceIndicies[prevIndex]].p);
      Vec3f_Subtract(&toPoint, pointToCheck, 
                               &points[faceIndicies[prevIndex]].p);
      Vec3f_Cross(&cross, &edge, &toPoint);
      dot = Vec3f_Dot(&cross, faceNormal);
      sign = GetSign(dot);      

      if(expectedSign == 0)
      {
         expectedSign = sign;
      } 
      else if(sign != 0 && sign != expectedSign)
      {
         return false; 
      }

      prevIndex = index;
   }
   return true;
}

static inline
bool RayCastToFace(const struct ray3f * ray,
                   const struct point * points,
                   const int * faceIndicies,
                   size_t faceIndiciesCount,
                   struct vec3f * outPoint)
{
   struct vec3f v1, v2, norm;

   Vec3f_Subtract(&v1, &points[faceIndicies[1]].p, &points[faceIndicies[0]].p); 
   Vec3f_Subtract(&v2, &points[faceIndicies[2]].p, &points[faceIndicies[0]].p); 
   Vec3f_Cross(&norm, &v1, &v2);
   float denom = Vec3f_Dot(&norm, &ray->dir);
   if(fabs(denom) > 0.00001)
   {
      // Ray is perpendicular to face
      struct vec3f planeIntersectionPoint;
      struct vec3f scaled;
      float t = -Vec3f_Dot(&ray->origin, &norm) / denom;
      if(t < 0)
      {
         return false;
      }
      Vec3f_Add(&planeIntersectionPoint, 
                &ray->origin, Vec3f_Scale(&scaled, &ray->dir, t));
      bool isPointInside = RayCastToFace_IsPointInsideFace(points, 
                                                           faceIndicies, 
                                                           faceIndiciesCount, 
                                                           &norm,
                                                           &planeIntersectionPoint);
      if(isPointInside && outPoint != NULL)
      {
         Vec3f_Copy(outPoint, &planeIntersectionPoint);
      }
      return isPointInside;

   }
   else
   {
      // Ray is co-plainer with face
      struct vec3f toPlane;
      Vec3f_Subtract(&toPlane, &points[faceIndicies[0]].p, &ray->origin);
      if(fabs(Vec3f_Dot(&toPlane, &norm)) > 0.00001)
      {
         // The origin of the ray is not on the plane. So we know it's not a hit
         return false;
      }
   }
   // TODO: This isn't right, there has to be a better way.
}

static inline
size_t Brush_ComputeNumberOfFacePoints(const int * faceIndicies)
{
   size_t faceIndiciesCount = 0;
   while(*faceIndicies >= 0)
   {
      faceIndiciesCount ++;
      faceIndicies ++;
   }
   return faceIndiciesCount;
}

static inline
bool Brush_RayCastFace(const struct brush * brush,
                       int faceStartIndex,
                       const struct ray3f * ray,
                       struct vec3f * point)
{
   const int *          indices = List_GetC_index(&brush->indices, NULL);
   const struct point * points  = List_GetC_point(&brush->points,  NULL);
   const int * faceIndicies = indices + faceStartIndex;
   size_t faceIndiciesCount = Brush_ComputeNumberOfFacePoints(faceIndicies);
   
   return RayCastToFace(ray, points, faceIndicies, faceIndiciesCount, point); 
}

void BVHNode_RayCast(const struct bvhNode * node, 
                     struct brushRayCastResults * results)
{
   if(AABB_RayCast(&node->aabb, results->ray))
   {
      if(BVHTree_IsLeaf(node))
      {
         const struct brush * brush = results->brush;
         int faceStartIndex = node->faceStartIndex;
         if(Brush_RayCastFace(brush, faceStartIndex, 
                              results->ray, &results->point))
         {
            results->hitCount ++;
            // TODO: Think about how to get closest point
         }
         
      }
      else
      {
         if(node->left != NULL)
         {
            BVHNode_RayCast(node->left, results);
         }
         if(node->right != NULL)
         {
            BVHNode_RayCast(node->right, results);
         }
      }
   }
}

struct brushRayCastResults * Brush_RayCast(const struct brush * brush, 
                                           const struct ray3f * ray, 
                                           struct brushRayCastResults * results)
{
   results->brush = brush;
   results->ray = ray;
   results->hitCount = 0;
   Vec3f_Zero(&results->point);
   
   BVHNode_RayCast(brush->root, results);

   return results;
}

void Brush_AddFace(struct brush * brush, const struct point * points, size_t pointsCount)
{
   size_t i;
   for(i = 0; i < pointsCount; i++)
   {
      size_t index;
      (void)List_Add_point(&brush->points, points[i], &index);
      (void)List_Add_index(&brush->indices, index, NULL);
   }
   (void)List_Add_index(&brush->indices, -1, NULL);
}

void Brush_Translate(struct brush * brush, const struct vec3f * by)
{
   size_t i, count;
   struct point * points = List_Get_point(&brush->points, &count);
   for(i = 0; i < count; i++)
   {
      Vec3f_Add(&points[i].p, &points[i].p, by);
   }
}

void Brush_WriteObj(const struct brush * brush, const char * filename)
{
   FILE * fh = fopen(filename, "w");
   size_t i, count;
   const struct point * points = List_GetC_point(&brush->points, &count);
   const int * indices;
   bool newRecord = true;
   for(i = 0; i < count; i++)
   {
      const struct vec3f * v = &points[i].p;
      fprintf(fh, "v %f %f %f\n", v->x, v->y, v->z);
   }
   fprintf(fh, "\n");

   for(i = 0; i < count; i++)
   {
      const struct vec3f * n = &points[i].n;
      fprintf(fh, "vn %f %f %f\n", n->x, n->y, n->z);
   }
   fprintf(fh, "\n");
   
   indices = List_GetC_index(&brush->indices, &count);
   for(i = 0; i < count; i++)
   {
      const int index = indices[i];
      if(newRecord)
      {
         newRecord = false;
         fprintf(fh, "f");
      }
      if(index == -1)
      {
         fprintf(fh, "\n");
         newRecord = true;
      }
      else
      {
         fprintf(fh, " %d//%d", index + 1, index + 1);
      } 
   }
   fprintf(fh, "\n");
   fclose(fh);
}

struct brush * Brush_Create_Cube(const struct vec3f * min, 
                                 const struct vec3f * max)
{
   struct brush * brush = Brush_Create();
   const struct point points[] = {
      // Top
      { { min->x, max->y, min->z }, {  0,  1,  0 }, { 0, 0, 0 } },
      { { min->x, max->y, max->z }, {  0,  1,  0 }, { 0, 0, 0 } },
      { { max->x, max->y, max->z }, {  0,  1,  0 }, { 0, 0, 0 } },
      { { max->x, max->y, min->z }, {  0,  1,  0 }, { 0, 0, 0 } },
      // Bottom
      { { min->x, min->y, min->z }, {  0, -1,  0 }, { 0, 0, 0 } },
      { { max->x, min->y, min->z }, {  0, -1,  0 }, { 0, 0, 0 } },
      { { max->x, min->y, max->z }, {  0, -1,  0 }, { 0, 0, 0 } },
      { { min->x, min->y, max->z }, {  0, -1,  0 }, { 0, 0, 0 } },
      // Front
      { { min->x, min->y, min->z }, {  0,  0, -1 }, { 0, 0, 0 } },
      { { min->x, max->y, min->z }, {  0,  0, -1 }, { 0, 0, 0 } },
      { { max->x, max->y, min->z }, {  0,  0, -1 }, { 0, 0, 0 } },
      { { max->x, min->y, min->z }, {  0,  0, -1 }, { 0, 0, 0 } },
      // Back
      { { min->x, min->y, max->z }, {  0,  0,  1 }, { 0, 0, 0 } },
      { { max->x, min->y, max->z }, {  0,  0,  1 }, { 0, 0, 0 } },
      { { max->x, max->y, max->z }, {  0,  0,  1 }, { 0, 0, 0 } },
      { { min->x, max->y, max->z }, {  0,  0,  1 }, { 0, 0, 0 } },
      // Left
      { { min->x, min->y, min->z }, { -1,  0,  0 }, { 0, 0, 0 } },
      { { min->x, min->y, max->z }, { -1,  0,  0 }, { 0, 0, 0 } },
      { { min->x, max->y, max->z }, { -1,  0,  0 }, { 0, 0, 0 } },
      { { min->x, max->y, min->z }, { -1,  0,  0 }, { 0, 0, 0 } },
      // Right
      { { max->x, min->y, min->z }, {  1,  0,  0 }, { 0, 0, 0 } },
      { { max->x, max->y, min->z }, {  1,  0,  0 }, { 0, 0, 0 } },
      { { max->x, max->y, max->z }, {  1,  0,  0 }, { 0, 0, 0 } },
      { { max->x, min->y, max->z }, {  1,  0,  0 }, { 0, 0, 0 } },
   };
   Brush_AddFace(brush, points + 0,  4);
   Brush_AddFace(brush, points + 4,  4);
   Brush_AddFace(brush, points + 8,  4);
   Brush_AddFace(brush, points + 12, 4);
   Brush_AddFace(brush, points + 16, 4);
   Brush_AddFace(brush, points + 20, 4);
   return brush;
}

#include <rmsymboltable.h>
RMM_SYMBOLTABLE_BEGIN(Brush)
   RMM_SYMBOLTABLE_FUNCTION(RayCastToFace)
RMM_SYMBOLTABLE_END()

