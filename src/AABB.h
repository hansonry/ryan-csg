#ifndef __AABB_H__
#define __AABB_H__

#include <stdbool.h>
#include <math.h>
#include "Vec3f.h"
#include "Ray3f.h"

struct aabb
{
   struct vec3f min;
   struct vec3f max;
};

enum aabbIntersection
{
   eAABBI_NoIntersection,
   eAABBI_Inside,
   eAABBI_Surrounds, 
   eAABBI_Intersection
};

static inline
struct aabb * AABB_Empty(struct aabb * aabb)
{
   Vec3f_Set(&aabb->min,  0,  0,  0);
   Vec3f_Set(&aabb->max, -1, -1, -1); 
   return aabb;
}

static inline
struct aabb * AABB_Set(struct aabb * aabb, float minX, float minY, float minZ,
                                           float maxX, float maxY, float maxZ)
{
   Vec3f_Set(&aabb->min, minX, minY, minZ);
   Vec3f_Set(&aabb->max, maxX, maxY, maxZ); 
   return aabb;
}

static inline
bool AABB_IsEmpty(const struct aabb * aabb)
{
   return aabb->min.x > aabb->max.x &&
          aabb->min.y > aabb->max.y &&
          aabb->min.z > aabb->max.z;
}

static inline
struct aabb * AABB_Copy(struct aabb * dest, const struct aabb * src)
{
   Vec3f_Copy(&dest->min, &src->min);
   Vec3f_Copy(&dest->max, &src->max);
   return dest;
}

static inline
struct aabb * AABB_AddPoint(struct aabb * aabb, const struct vec3f * point)
{
   if(AABB_IsEmpty(aabb))
   {
      aabb->min.x = point->x;
      aabb->min.y = point->y;
      aabb->min.z = point->z;

      aabb->max.x = point->x;
      aabb->max.y = point->y;
      aabb->max.z = point->z;
   }
   else
   {
      if(aabb->min.x > point->x) aabb->min.x = point->x;
      if(aabb->min.y > point->y) aabb->min.y = point->y;
      if(aabb->min.z > point->z) aabb->min.z = point->z;

      if(aabb->max.x < point->x) aabb->max.x = point->x;
      if(aabb->max.y < point->y) aabb->max.y = point->y;
      if(aabb->max.z < point->z) aabb->max.z = point->z;
   }
   return aabb;
}

static inline
struct aabb * AABB_Merge(struct aabb * out, const struct aabb * a, 
                                            const struct aabb * b)
{
   bool aEmpty = AABB_IsEmpty(a);
   bool bEmpty = AABB_IsEmpty(b);

   if(aEmpty && bEmpty)
   {
      return AABB_Empty(out);
   }
   if(aEmpty)
   {
      if(out == b)
      {
         return out;
      }
      return AABB_Copy(out, b);
   }
   if(bEmpty)
   {
      if(out == a)
      {
         return out;
      }
      return AABB_Copy(out, a);
   }


   if(a == out)
   {
      AABB_AddPoint(out, &b->min);
      AABB_AddPoint(out, &b->max);
      return out;
   }
   if(b == out)
   {
      AABB_AddPoint(out, &a->min);
      AABB_AddPoint(out, &a->max);
      return out;
   }
   AABB_Copy(out, a);
   AABB_AddPoint(out, &b->min);
   AABB_AddPoint(out, &b->max);
   return out;
}


static inline
bool AABB_ContainsPoint(const struct aabb * aabb, const struct vec3f * p)
{
   return p->x >= aabb->min.x && p->x <= aabb->max.x &&
          p->y >= aabb->min.y && p->y <= aabb->max.y &&
          p->z >= aabb->min.z && p->z <= aabb->max.z;
}

static inline
enum aabbIntersection AABB_Intersection(const struct aabb * a, 
                                        const struct aabb * b)
{
   if(AABB_IsEmpty(a) || AABB_IsEmpty(b))
   {
      return eAABBI_NoIntersection;
   }


   if(a->min.x <= b->max.x && a->max.x >= b->min.x &&
      a->min.y <= b->max.y && a->max.y >= b->min.y && 
      a->min.z <= b->max.z && a->max.z >= b->min.z)
   {
      bool cMinAB = AABB_ContainsPoint(a, &b->min);
      bool cMaxAB = AABB_ContainsPoint(a, &b->max);
      
      if(cMinAB && cMaxAB)
      {
         return eAABBI_Surrounds;
      }
      bool cMinBA = AABB_ContainsPoint(b, &a->min);
      bool cMaxBA = AABB_ContainsPoint(b, &a->max);

      if(cMinBA && cMaxBA)
      {
         return eAABBI_Inside;
      }
      return eAABBI_Intersection;
   }

   return eAABBI_NoIntersection;
}


static inline
void AABB_SplitOnLongestAxis(const struct aabb * src, struct aabb * a, 
                                                      struct aabb * b)
{
   struct vec3f diff;
   Vec3f_Subtract(&diff, &src->max, &src->min);
   int longestAxis = 0;
   float axisDiff = diff.x;
   float halfAxisDiff;
   if(axisDiff < diff.y)
   {
      axisDiff = diff.y;
      longestAxis = 1;
   }
   if(axisDiff < diff.z)
   {
      axisDiff = diff.z;
      longestAxis = 2;
   }

   AABB_Copy(a, src);
   AABB_Copy(b, src);
   halfAxisDiff = axisDiff / 2.0f;

   switch(longestAxis)
   {
   case 0: // x
      a->max.x = a->min.x + halfAxisDiff;
      b->min.x = b->max.x - halfAxisDiff;
      break;
   case 1: // y
      a->max.y = a->min.y + halfAxisDiff;
      b->min.y = b->max.y - halfAxisDiff;
      break;
   case 2: // z
      a->max.z = a->min.z + halfAxisDiff;
      b->min.z = b->max.z - halfAxisDiff;
      break;
   }
}

static inline
bool AABB_RayCast(const struct aabb * aabb, const struct ray3f * ray)
{
   float dirIX = 1 / (double)ray->dir.x;
   float dirIY = 1 / (double)ray->dir.y;
   float dirIZ = 1 / (double)ray->dir.z;
   
   bool sign = false;
   float bmin, bmax;
   float dmin, dmax;
   const struct vec3f *corners[2] = {&aabb->min, &aabb->max};

   float tmin = 0.0;
   float tmax = INFINITY;

   // X
   sign = signbit(dirIX);
   bmin = corners[sign]->x;
   bmax = corners[!sign]->x;
   dmin = (bmin - ray->origin.x) * dirIX;
   dmax = (bmax - ray->origin.x) * dirIX;
   tmin = fmaxf(dmin, tmin);
   tmax = fminf(dmax, tmax);

   // Y
   sign = signbit(dirIY);
   bmin = corners[sign]->y;
   bmax = corners[!sign]->y;
   dmin = (bmin - ray->origin.y) * dirIY;
   dmax = (bmax - ray->origin.y) * dirIY;
   tmin = fmaxf(dmin, tmin);
   tmax = fminf(dmax, tmax);

   // Y
   sign = signbit(dirIZ);
   bmin = corners[sign]->z;
   bmax = corners[!sign]->z;
   dmin = (bmin - ray->origin.z) * dirIZ;
   dmax = (bmax - ray->origin.z) * dirIZ;
   tmin = fmaxf(dmin, tmin);
   tmax = fminf(dmax, tmax);

   return tmin < tmax;
}


#endif // __AABB_H__

