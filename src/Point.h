#ifndef __POINT_H__
#define __POINT_H__


#include "Vec3f.h"

struct point
{
   struct vec3f p;
   struct vec3f n;
   struct vec3f uv;
};

#endif // __POINT_H__

